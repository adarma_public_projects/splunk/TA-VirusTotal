import { 
    get_generic_data, 
    get_proxy_data, 
    get_vt_functionalities
} from './resources/constants.js'

// ----------------------------------
// Functions defining UI components
// ----------------------------------

function get_checkbox(id) {
    var name = id.replace('_id', '');

    const template_string =
        "   <div class='control shared-controls-syntheticcheckboxcontrol control-default' data-name='" + name + "'> " +
        "     <span class='uneditable-input ' data-role='uneditable-input' style='display:none'></span>" +
        "     <input type='checkbox' id='" + id + "' name='" + name + "' class='checkbox'>" +
        "   </div>";

    return template_string;
}


function get_field(id, value, type) {
    var name = id.replace('_id', '');

    const template_string =
        "   <div class='control shared-controls-textareacontrol control-default' data-name='" + name + "'>" +
        "     <span class='uneditable-input uneditable-input-multiline' style='display:none'></span>" +
        "     <input type='" + type + "' id='" + id + "' name='" + name + "' value='" + value + "'>" +
        "   </div>";

    return  template_string;
}


function get_help(text, restyle=false) {

    const template_string =
        "   <div class='help-block' " + (restyle == true ? "style='margin-left:10px;'" : "") + ">" +
        "     <span>" + text + "</span>" +
        "   </div>";

    return template_string;
}

// ----------------------------------
// Functions defining main sections
// ----------------------------------

function get_generic() {
    var template_string = "<h1>Generic</h1>";
    let generic_content = get_generic_data();

    generic_content.forEach((field) => {
        template_string = template_string +
        "        <div class='field " + field.input.id.replace('_id', '') + "'>" +
        "            <div class='title'>" +
        "                <div>" +
        "                    <h3>" + field.title + "</h3>" +
        "                    <p>" + field.description.replaceAll('\n', '</p><p>') + "</p>" +
        "                </div>" +
        "            </div>" +
        "            </br>" +
        "           <div class='form-horizontal control-group shared-controls-controlgroup control-group-default'>" +
        "               <label class='control-label' for='" + field.input.id + "'>" + field.input.label + "</label>" +
        "               <div role='group' class='controls controls-join'>" +
                            get_field(field.input.id, field.input.value, field.input.type) +
        "               </div>" +
                        (field.input.help === "" ? "" : get_help(field.input.help)) +
        "            </div>" +
        "        </div>";
    });

    return template_string;
}


function get_proxy_template() {
    let proxy_obj = get_proxy_data();
    let proxy = {};
    for (var i=1; i <= proxy_obj.inputs.length; i++) {
        proxy[i.toString()] = proxy_obj.inputs.filter(obj => {
            return obj.uid === i.toString();
        })[0];
    }

    const template_string =
        "        <h1>Proxy</h1>" +
        "        <div>" +
        "           <p>" + proxy_obj.description + "</p>" +
        "        </div>" +
        "        <div class='styleguide-forms-section form-complex'>" +
        "           <fieldset id='form_section_proxy' class=''>" +
        "              <div class='form-horizontal control-group shared-controls-controlgroup control-group-default'>" +
        "                 <label class='control-label' for='" + proxy["1"].id + "'>" + proxy["1"].label + "</label>" +
        "                 <div role='group' class='controls controls-join'>" +
                            get_checkbox(proxy["1"].id) +
        "                 </div>" +
        "              </div>" +
        "              <div class='form-horizontal control-group control-group-toggle shared-controls-controlgroup control-group-default hide'>" +
        "                 <label class='control-label' for='" + proxy["2"].id + "'>" + proxy["2"].label + "</label>" +
        "                 <div role='group' class='controls controls-join '>" +
                            get_field(proxy["2"].id, '', 'text') +
        "                 </div>" +
                          get_help(proxy["2"].help) +
        "              </div>" +
        "              <div class='form-horizontal control-group control-group-toggle shared-controls-controlgroup control-group-default hide'>" +
        "                 <label class='control-label' for='" + proxy["3"].id + "'>" + proxy["3"].label + "</label>" +
        "                 <div role='group' class='controls controls-join'>" +
                            get_field(proxy["3"].id, '', 'text') +
        "                 </div>" +
                          get_help(proxy["3"].help) +
        "              </div>" +
        "              <div class='form-horizontal control-group control-group-toggle shared-controls-controlgroup control-group-default hide'>" +
        "                 <label class='control-label' for='" + proxy["4"].id + "'>" + proxy["4"].label + "</label>" +
        "                 <div role='group' class='controls controls-join'>" +
                                get_field(proxy["4"].id, '', 'password') +
        "                 </div>" +
                          get_help(proxy["4"].help) +
        "              </div>" +
        "           </fieldset>" +
        "        </div>";

    return template_string;
}


function get_vt_subsections(subsections) {
    var template_string = "";

    subsections.forEach((subsection) => {
        template_string = template_string +
            "      <div class='form-horizontal control-group control-group-toggle shared-controls-controlgroup control-group-default'>" +
            "          <div>" +
            "            <h3>" + subsection.title + "</h3>" +
            "            <p>" + subsection.description.replaceAll('\n', '</p><p>') + "</p>" +
            "          </div>" +
            "          <label class='control-label' for='" + subsection.input.id + "'>" + subsection.input.label + "</label>" +
            "          <div role='group' class='controls controls-join '>" +
            (subsection.input.is_checkbox == true ?
                get_checkbox(subsection.input.id) : get_field(subsection.input.id, subsection.input.value, subsection.input.type)) +
            (subsection.input.help === "" ? "" : get_help(subsection.input.help, true)) +
            "          </div>" +
            "      </div>";
    });

    return template_string;
}


function get_vt_functionalities_template() {
    let vt_content = get_vt_functionalities();
    let template_string =
        "        <h1>" + vt_content.title + "</h1>" +
        "        <div>" +
        "           <p>" + vt_content.description + "</p>" +
        "        </div>" + 
        "        <div class='accordion'>";
    
    vt_content.sections.forEach((section) => {
        template_string = template_string +
            "    <div class='accordion-group'>" +
            "        <div class='accordion-heading active'><a class='accordion-toggle'><i class='icon-accordion-toggle icon-triangle-down-small'></i>" + section.name + "</a></div>" +
            "        <div class='accordion-body' " + (section.id > parseInt("1") ? "style='display: none;'" : "") + ">" +
            "            <div class='accordion-inner'>" +
            "                <div class='styleguide-forms-section form-complex'>" +
            "                   <fieldset id='" + section.label + "' class=''>" +
                                    get_vt_subsections(section.content) +
            "                    </fieldset>" +
            "                </div>" +
            "            </div>" +
            "        </div>" +
            "    </div>";
    });
    template_string = template_string + "</div>";

    return template_string;
}

// ----------------------------------
// Main UI template definition
// ----------------------------------

function get_template() {

    const template_string =
        "<div class='setup container'>" +
        "    <div class='left'>" +
                get_generic() +
                get_proxy_template() + 
                get_vt_functionalities_template() +
        "        <div class='modal-footer'>" +
        "            <button name='cancel_button' class='btn btn-default'>Cancel</button>" +
        "            <button name='save_button' class='btn btn-primary'>Save</button>" +
        "        </div>" +
        "        <br/>" +
        "        <div class='error output'></div>" +
        "    </div>" +
        "</div>";

    return template_string;
}

export default get_template;
